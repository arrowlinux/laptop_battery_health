#!/bin/bash

# Created By: Elias W / eliasw4u
#------------------------------------------------------------------------------
# Created on 05/Jul/2020 undergoing development to date.
#------------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# visit
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
###############################################################################
####################### Acknowledgements ######################################
###############################################################################
# A special thanks For code contributions 
# and concept ideas goes to
# rickromig, lion.p, and deck_luck
# on Ezeetalk.com
###############################################################################
## Known Bugs ##
# none. 
###############################################################################
## Basic Variable Info ##
# This piece of code comes from rickromig ... thanks.
readonly SCRIPT=$(basename "$0")
readonly VERSION="2.1.4"
readonly UPDATED="06 Aug 2020"
#################################
# This piece of code comes from lion.p ... thanks.
for name in $(ls /sys/class/power_supply | grep BAT)
do
    Battery_Name=${name}
done
#################################
clear;clear

# colors
  c1="\e[31m" # Red
  c2="\e[32m" # Light Green
  c3="\e[33m" # Yellow
  c4="\e[34m"  # Calm Blue
  c5="\e[35m" # Violet 
  c6="\e[36m" # Arch Blue
  c7="\e[37m" # White
  c8="\e[38m" # Sky Blue, Sometimes White
  c9="\e[39m" # Light Grey
  c10="\e[30m" # Dark Grey
  c20="\e[0m" # Normal

# Unicode variables
# Words
Weak_bold="$(echo -e "\U1F1FC \U1F1EA \U1F1E6 \U1F1F0")"
very_Weak_bold="$(echo -e "\U1F1FB \U1F1EA \U1F1F7 \U1F1FE,  \U1F1FC \U1F1EA \U1F1E6 \U1F1F0")"
Replace_bold="$(echo -e "\U1F1F7 \U1F1EA \U1F1F5 \U1F1F1 \U1F1E6 \U1F1E8 \U1F1EA")"
Battery_bold="$(echo -e "\U1F1E7 \U1F1E6 \U1F1F9 \U1F1F9 \U1F1EA \U1F1F7 \U1F1FE ")"
# Icons
Tri_Warn="$(echo -e "\U26A0\UFE0F")"
No_Warn="$(echo -e "\U26D4")"
###

# warning message wrapper function
warn()
{
  echo >&2 "$(tput bold; tput setaf 3)[!] ${*}$(tput sgr0)"
}
warn2()
{
  echo >&2 "$(tput bold; tput setaf 3)    ${*}$(tput sgr0)"
}
###
main_variables(){
for Form2 in $(ls /sys/class/power_supply/${Battery_Name} | grep "full" | cut -d "_" -f1)
do
    the_word=${Form2}
done
TWF="${the_word}_full"
TWFD="${the_word}_full_design"
if [ -f /sys/class/power_supply/${Battery_Name}/${the_word}_full ]
     then 
         battery_percintage
    else
        No_Battery
        NO_BTTRY="+"
fi
}

Bat_Cap(){
if [ -f /sys/class/power_supply/${Battery_Name}/capacity ]
   then
   BaT_caP="$( cat /sys/class/power_supply/${Battery_Name}/capacity )"
       echo -e "\nBattery Charge: ${BaT_caP}%"
fi
}
Bat_Stat(){
if [ -f /sys/class/power_supply/${Battery_Name}/status ]
   then
       BaT_stat="$( cat /sys/class/power_supply/${Battery_Name}/status )"
       echo -e "Battery Status: ${BaT_stat} \n"
fi
}

battery_percintage(){
  var3=" "
BaT_aCtUaL="$( awk 'NF{print $1/1000000}' OFMT="%2.2f" /sys/class/power_supply/${Battery_Name}/${TWF} )"
BaT_dEsIgN="$( awk 'NF{print $1/1000000}' OFMT="%2.2f" /sys/class/power_supply/${Battery_Name}/${TWFD} )"
PerC="$( echo "100/${BaT_dEsIgN}*${BaT_aCtUaL}" | bc -l | awk '{print $1/1}' OFMT="%.2f" ) "
PerC2="$( echo "100/${BaT_dEsIgN}*${BaT_aCtUaL}" | bc -l | awk '{print $1/1}' OFMT="%.0f" ) "
}

No_Battery(){
          PerC="0"
          PerC2="0"
          BaT_aCtUaL="0"
          BaT_dEsIgN="0"
}

no_bat_warning(){
echo ""
warn "The battery is not installed,"
warn2 "Or no longer functions."
}

battery_info_colour(){
if [ "$PerC2" -le "0" ]
   then
       Co1or="$c1" # c1 is Red
       var3="\n${No_Warn}  ${Replace_bold}. the ${Battery_bold}. ${No_Warn}\n"
       NO_BTTRY="+"
   elif [ "$PerC2" -le "35" ]
        then
            Co1or="$c1" # c1 is Red
            var3="\n${No_Warn}  ${Replace_bold}. the ${Battery_bold}. ${No_Warn}\n"
   elif [ "$PerC2" -le "50" ]
        then
            Co1or="$c3" # c3 is Yellow
            var3="\n${Tri_Warn}.  ${Battery_bold} is ${very_Weak_bold}! ${Tri_Warn}\n"
   elif [ "$PerC2" -le "65" ]
        then
             Co1or="$c3" # c3 is Yellow
             var3="\n${Tri_Warn}.  ${Battery_bold} is getting ${Weak_bold}! ${Tri_Warn}\n"
        else
            Co1or="$c2" # c2 is Green
fi
}

Wat_or_amp_hours(){
case $the_word in
  energy) 
         W_A_h="Wh" ;;
  charge) 
         W_A_h="Ah" ;;  

esac
}

Bat_Cap
Bat_Stat
main_variables
battery_info_colour
Wat_or_amp_hours
c30="$Co1or"

echo -e "${c4}  BATTERY CONDITION ${c20}\n\n${c6}DESIGN MAX: ${c2}${BaT_dEsIgN} ${W_A_h} ${c6}\nACTUAL MAX: ${c2}${BaT_aCtUaL} ${W_A_h} \n${c6}BAT HEALTH: ${c30}${PerC}%${c20}"

if [ "${NO_BTTRY}" = "+" ]
  then
      no_bat_warning
fi

echo -e ${var3}
echo $"$SCRIPT V$VERSION"
echo ""
